/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/12/18/14:09
 * 项目名称: risk
 * 文件名称: Publisher.java
 * 文件描述: publisher
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.learn.zmq.pubsub;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * 包名称：com.ysstech.zmq.pubsub
 * 类名称：Publisher
 * 类描述：publisher
 * 创建人：@author fengxin
 * 创建时间：2019/12/18/14:09
 */
public class Publisher {

    public static void main(String[] args) throws Exception
    {
        // Prepare our context and publisher
        try (ZContext context = new ZContext()) {
            ZMQ.Socket publisher = context.createSocket(SocketType.PUB);
            publisher.bind("tcp://*:5563");

            while (!Thread.currentThread().isInterrupted()) {
                // Write two messages, each with an envelope and content
                publisher.sendMore("A");
                publisher.send("We don't want to see this");
                publisher.sendMore("B");
                publisher.send("We would like to see this");

                Thread.sleep(3000);

            }
        }
    }

}
