/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/12/18/14:10
 * 项目名称: risk
 * 文件名称: Subscriber.java
 * 文件描述: subscriber
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.learn.zmq.pubsub;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * 包名称：com.ysstech.zmq.pubsub
 * 类名称：Subscriber
 * 类描述：subscriber
 * 创建人：@author fengxin
 * 创建时间：2019/12/18/14:10
 */
public class Subscriber {

    public static void main(String[] args)
    {
        // Prepare our context and subscriber
        try (ZContext context = new ZContext()) {
            ZMQ.Socket subscriber = context.createSocket(SocketType.SUB);
            subscriber.connect("tcp://localhost:5563");

            // 订阅topic A
//            subscriber.subscribe("A".getBytes(ZMQ.CHARSET));

            // 订阅topic B
            subscriber.subscribe("B".getBytes(ZMQ.CHARSET));

            while (!Thread.currentThread().isInterrupted()) {
                // Read envelope with address
                String topic = subscriber.recvStr();
                // Read message contents
                String contents = subscriber.recvStr();
                System.out.println(topic + " : " + contents);
            }
        }
    }
}
