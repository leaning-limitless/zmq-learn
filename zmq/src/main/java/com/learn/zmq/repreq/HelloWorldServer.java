/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/12/18/10:30
 * 项目名称: risk
 * 文件名称: HelloWordServer.java
 * 文件描述: zmq helloworld 
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.learn.zmq.repreq;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * 包名称：com.ysstech.zmq
 * 类名称：HelloWordServer
 * 类描述：zmq helloworld
 * 创建人：@author fengxin
 * 创建时间：2019/12/18/10:30
 */

public class HelloWorldServer {
        public static void main(String[] args) throws Exception
        {
            try (ZContext context = new ZContext()) {
                // 响应客户端请求的socket
                ZMQ.Socket socket = context.createSocket(SocketType.REP);
                int port = 5555;
                socket.bind(String.format("tcp://*:%d",port));
                System.out.println(String.format("bind to port %d",port));

                while (!Thread.currentThread().isInterrupted()) {

                    // 阻塞直到收到消息
                    byte[] reply = socket.recv(0);

                    // 打印消息
                    System.out.println(
                            "Received: [" + new String(reply, ZMQ.CHARSET) + "]"
                    );

                    // 发送响应
                    String response = port+" Hello, Client!";
                    socket.send(response.getBytes(ZMQ.CHARSET), 0);

                }
            }
        }
    }