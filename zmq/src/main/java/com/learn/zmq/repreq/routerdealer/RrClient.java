package com.learn.zmq.repreq.routerdealer;

import org.zeromq.*;
import org.zeromq.ZMQ.Socket;

/**
* Hello World client
* Connects REQ socket to tcp://localhost:5559
* Sends "Hello" to server, expects "World" back
*/
public class RrClient
{

    public static void main(String[] args)
    {
        try (ZContext context = new ZContext()) {
            //  Socket to talk to server
            Socket requester = context.createSocket(SocketType.REQ);
            requester.connect("tcp://localhost:5559");

            System.out.println("launch and connect client.");

            for (int request_nbr = 0; request_nbr < 10; request_nbr++) {
                requester.send("Hello", 0);
                String reply = requester.recvStr(0);
                System.out.println(
                    "Received reply " + request_nbr + " [" + reply + "]"
                );

                ZMQ.sleep(1);

            }
        }
    }
}
