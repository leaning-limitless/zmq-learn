/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/12/18/13:41
 * 项目名称: risk
 * 文件名称: HelloWorldClient.java
 * 文件描述: 
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.learn.zmq.repreq;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * 包名称：com.ysstech.zmq
 * 类名称：HelloWorldClient
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/12/18/13:41
 */
public class HelloWorldClient {
    public static void main(String[] args) throws Exception
    {
        try (ZContext context = new ZContext()) {
            // 向服务器发送请求的socket
            ZMQ.Socket socket = context.createSocket(SocketType.REQ);
            socket.connect("tcp://localhost:5555");
//            socket.connect("tcp://localhost:5556");

            int msgNo = 0;
            while (!Thread.currentThread().isInterrupted()) {


                msgNo++;
                // 发送请求
                String request = String.format("%d Hello, Server!",msgNo);
                socket.send(request.getBytes(ZMQ.CHARSET), 0);


                // flags为0  意味着一直阻塞直到收到消息
                byte[] reply = socket.recv(0);

                // 打印消息
                System.out.println(
                        "Received: [" + new String(reply, ZMQ.CHARSET) + "]"
                );

                Thread.sleep(1000);
            }
        }
    }
}
