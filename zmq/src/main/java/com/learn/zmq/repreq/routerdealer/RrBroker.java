/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/12/18/15:49
 * 项目名称: risk
 * 文件名称: routerdealer.java
 * 文件描述: router dealer
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.learn.zmq.repreq.routerdealer;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Poller;

/**
 * 包名称：com.ysstech.zmq
 * 类名称：RrBroker
 * 类描述：router dealer
 * 创建人：@author fengxin
 * 创建时间：2019/12/18/15:49
 */
public class RrBroker {

    public static void main(String[] args)
    {
        //  Prepare our context and sockets
        try (ZContext context = new ZContext()) {
            ZMQ.Socket frontend = context.createSocket(SocketType.ROUTER);
            ZMQ.Socket backend = context.createSocket(SocketType.DEALER);
            frontend.bind("tcp://*:5559");
            backend.bind("tcp://*:5560");

            System.out.println("launch and connect broker.");

            //  Initialize poll set
            ZMQ.Poller items = context.createPoller(2);
            items.register(frontend, Poller.POLLIN);
            items.register(backend, Poller.POLLIN);

            boolean more = false;
            byte[] message;

            //  Switch messages between sockets
            while (!Thread.currentThread().isInterrupted()) {
                //  poll and memorize multipart detection
                items.poll();

                if (items.pollin(0)) {
                    while (true) {
                        // receive message
                        message = frontend.recv(0);
                        more = frontend.hasReceiveMore();

                        // Broker it
                        backend.send(message, more ? ZMQ.SNDMORE : 0);
                        if (!more) {
                            break;
                        }
                    }
                }

                if (items.pollin(1)) {
                    while (true) {
                        // receive message
                        message = backend.recv(0);
                        more = backend.hasReceiveMore();
                        // Broker it
                        frontend.send(message, more ? ZMQ.SNDMORE : 0);
                        if (!more) {
                            break;
                        }
                    }
                }
            }
        }
    }




}
