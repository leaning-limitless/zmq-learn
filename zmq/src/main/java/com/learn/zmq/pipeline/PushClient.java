/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/12/18/14:32
 * 项目名称: risk
 * 文件名称: PushClient.java
 * 文件描述: pushclient
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.learn.zmq.pipeline;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * 包名称：com.ysstech.zmq.pushpull
 * 类名称：PushClient
 * 类描述：pushclient
 * 创建人：@author fengxin
 * 创建时间：2019/12/18/14:32
 */
public class PushClient {
    public static void main(String[] args) throws Exception {
        // Prepare our context and pusher
        try (ZContext context = new ZContext()) {

            //  Socket to send messages on
            ZMQ.Socket sender = context.createSocket(SocketType.PUSH);
            sender.connect("tcp://localhost:5567");

            while (!Thread.currentThread().isInterrupted()) {
                // Send a request
                String request = "Hello, Server!";
                sender.send(request.getBytes(ZMQ.CHARSET), 0);
                Thread.sleep(3000);
            }

        }
    }
}
