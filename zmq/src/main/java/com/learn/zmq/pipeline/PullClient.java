/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/12/18/14:26
 * 项目名称: risk
 * 文件名称: Puller.java
 * 文件描述: puller
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.learn.zmq.pipeline;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * 包名称：com.ysstech.zmq.pushpull
 * 类名称：Puller
 * 类描述：puller
 * 创建人：@author fengxin
 * 创建时间：2019/12/18/14:26
 */
public class PullClient {
    public static void main(String[] args) throws Exception {
        // Prepare our context and pusher
        try (ZContext context = new ZContext()) {

            //  Socket to pull messages on
            ZMQ.Socket puller = context.createSocket(SocketType.PULL);
            puller.connect("tcp://localhost:5557");

            while (!Thread.currentThread().isInterrupted()) {

                // Block until a message is received
                byte[] reply = puller.recv(0);

                // Print the message
                System.out.println(
                        "Received: [" + new String(reply, ZMQ.CHARSET) + "]"
                );
            }


        }
    }
}
